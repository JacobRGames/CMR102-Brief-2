using TMPro;
using UnityEngine;

public class Operator : MonoBehaviour
{
    [Tooltip("Example: + | - | x | /")]
    [SerializeField] private char operatorType;

    [SerializeField] private TextMeshProUGUI outputText;
    private float output;

    [HideInInspector] public CardSideCheck leftSideCheck;
    [HideInInspector] public CardSideCheck rightSideCheck;

    // Update is called once per frame
    private void Update()
    {
        if (leftSideCheck?.numberCard != null && rightSideCheck?.numberCard != null)
        {
            output = Calculator.Calculate(leftSideCheck.numberCard.cardNumber, rightSideCheck.numberCard.cardNumber, operatorType.ToString());
            outputText.text = output.ToString();
        }
    }
}